const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Buyer = sequelize.define('Buyer', {

    name:{
        type: DataTypes.STRING,
        allowNull: false
    },

    cpf:{
        type: DataTypes.STRING,
        allowNull: false
    },
    
    email:{
        type: DataTypes.STRING,
        allowNull: false
    },

    password:{
        type: DataTypes.STRING,
        allowNull: false
    },

    address:{
        type: DataTypes.STRING,
        allowNull: false
    },

    cellphoneNumber:{
        type: DataTypes.STRING,
        allowNull: false
    },
    
    birthdate:{
        type: DataTypes.DATEONLY,
        allowNull: false
    },

});

Buyer.associate = function(models) {
    Buyer.hasMany(models.Product);
};

module.exports = Buyer;
