const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {

    name:{
        type: DataTypes.STRING,
        allowNull: false
    },

    image:{
        type: DataTypes.TEXT,
        allowNull:false
    },

    price:{
        type: DataTypes.DOUBLE,
        allowNull: false
    },

    description:{
        type: DataTypes.STRING,
        allowNull:false
    },
    
    rating:{
        type: DataTypes.STRING,
        allowNull: false
    },

    quantity:{
        type: DataTypes.INTEGER,
        allowNull: false
    },
    
});

Product.associate = function(models) {
    Product.belongsTo(models.Buyer);
    Product.belongsTo(models.Seller);
};

module.exports = Product;
