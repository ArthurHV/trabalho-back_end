const {Router} = require("express");

const router = Router();

const BuyerController = require("../controllers/BuyerController");
const ProductController = require("../controllers/ProductController");
const SellerController = require("../controllers/SellerController");

router.post("/Buyer", BuyerController.create);
router.get("/Buyer/:id", BuyerController.show); 
router.get("/Buyer", BuyerController.index); 
router.put("/Buyer/:id", BuyerController.update);
router.delete("/Buyer/:id", BuyerController.destroy);

router.post("/Seller", SellerController.create);
router.get("/Seller/:id", SellerController.show); 
router.get("/Seller", SellerController.index); 
router.put("/Seller/:id", SellerController.update);
router.delete("/Seller/:id", SellerController.destroy);

router.post("/Product", ProductController.create);
router.get("/Product/:id", ProductController.show); 
router.get("/Product", ProductController.index); 
router.put("/Product/:id", ProductController.update);
router.delete("/Product/:id", ProductController.destroy);
router.put("/Product/purchase/:id",ProductController.purchase);
router.put("/Product/cancelPurchase/:id",ProductController.cancelPurchase);


module.exports = router;

