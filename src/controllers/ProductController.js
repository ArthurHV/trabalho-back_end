const {Op} = require('sequelize');
const Product = require('../models/Product');
const Buyer = require('../models/Buyer');
const Seller = require('../models/Seller');

const create = async(req,res) => {

    try {
        const product = await Product.create(req.body);
        console.log(req.body);
        return res.status(200).json({msg: "Produto foi cadastrado.", product: product});

    } catch(err) {
        res.status(500).json({error: err});
    }
};

const show = async(req,res) => {

    const {id} = req.params;

    try {
        const product = await Product.findByPk(id);
        return res.status(200).json({product});

    }catch(err){
        return res.status(500).json({err});
    }
};

const index = async(req, res) =>{

    try{
        const product = await Product.findAll();
        return res.status(200).json({product});

    } catch(err){
        return res.status(500).json({err});
    }
};

const destroy = async(req,res) => {

    const {id} = req.params;

    try {
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Produto foi deletado.");
        }
        throw new Error ();

    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

const update = async(req,res) => {

    const {id} = req.params;

    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send({product});
        } 
        throw new Error();

    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

const purchase = async(req,res) => {

    const {id} = req.params;

    try {
        const product = await Product.findByPk(id);
        const buyer = await Buyer.findByPk(req.body.buyerId);
        const seller = await Seller.findByPk(req.body.sellerId);
        await product.setBuyer(buyer);
        await product.setSeller(seller);
        return res.status(200).json(product);

    }catch(err){
        return res.status(500).json({err});
    }
}

const cancelPurchase = async(req,res) => {

    const {id} = req.params;

    try {
        const product = await Product.findByPk(id);
        await product.setBuyer(null);
        await product.setSeller(null);
        return res.status(200).json(product);

    }catch(err){
        return res.status(500).json({err});
    }
}


module.exports = {
    create,
    show,
    index,
    destroy,
    update,
    purchase,
    cancelPurchase,
}

