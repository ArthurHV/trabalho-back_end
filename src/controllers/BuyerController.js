const {Op} = require('sequelize');
const Buyer = require('../models/Buyer');

const create = async(req,res) => {

    try {
        const buyer = await Buyer.create(req.body);
        console.log(req.body);
        return res.status(200).json({msg: "Comprador foi cadastrado.", buyer: buyer});

    } catch(err) {
        res.status(500).json({error: err});
    }
};

const show = async(req,res) => {

    const {id} = req.params;

    try {
        const buyer = await Buyer.findByPk(id);
        return res.status(200).json({buyer});

    }catch(err){
        return res.status(500).json({err});
    }
};

const index = async(req, res) =>{

    try{
        const buyer = await Buyer.findAll();
        return res.status(200).json({buyer});

    } catch(err){
        return res.status(500).json({err});
    }
};

const destroy = async(req,res) => {

    const {id} = req.params;

    try {
        const deleted = await Buyer.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Comprador foi deletado.");
        }
        throw new Error ();

    }catch(err){
        return res.status(500).json("Comprador não encontrado.");
    }
};

const update = async(req,res) => {

    const {id} = req.params;

    try {
        const [updated] = await Buyer.update(req.body, {where: {id: id}});
        if(updated) {
            const buyer = await Buyer.findByPk(id);
            return res.status(200).send(buyer);
        } 
        throw new Error();

    }catch(err){
        return res.status(500).json("Comprador não encontrado.");
    }
};

module.exports = {
    create,
    show,
    index,
    destroy,
    update,
}

